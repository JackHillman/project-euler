import unittest
from src.p2 import Main

class TestProblem2(unittest.TestCase):

    def test_aux(self):
        self.assertEqual(
            Main(10, lambda x: True).answer(),
            19
        )

    def test_main(self):
        self.assertEqual(
            Main(4000000, lambda x: x % 2 == 0).answer(),
            4613732
        )
