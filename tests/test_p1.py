import unittest
from src.p1 import Main

class TestProblem1(unittest.TestCase):

    def test_example(self):
        self.assertEqual(
            Main(10, [3, 5]).answer(),
            23
        )

    def test_main(self):
        self.assertEqual(
            Main(1000, [3, 5]).answer(),
            233168
        )
