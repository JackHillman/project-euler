[![build status](https://gitlab.com/JackHillman/project-euler/badges/master/build.svg)](https://gitlab.com/JackHillman/project-euler/commits/master)

# About
This project attempts to solve the problems over at [https://projecteuler.net/](https://projecteuler.net/)
