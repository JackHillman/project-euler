"""
Each new term in the Fibonacci sequence is generated by adding the
previous two terms. By starting with 1 and 2, the first 10 terms will be:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

By considering the terms in the Fibonacci sequence whose values do not exceed
four million, find the sum of the even-valued terms.
"""

class Main:

    """
    Our constraints are as follows:
        Limit:
            Our task only requires us to find the even values up to and
            including four million (4,000,000 or 4x10^6)

        Match:
            Our task also requires us to only sum the even valued integers
    """
    def __init__(self, limit, match):
        self.limit = limit
        self.match = match
        self.fib = {'a': 0, 'b': 1}

    def fibonacci(self):
        while True:
            result = self.fib['a'] + self.fib['b']

            self.fib['a'] = self.fib['b']
            self.fib['b'] = result

            yield result

    def answer(self):
        matches = []
        fib = self.fibonacci()

        current = next(fib)
        while current <= self.limit:
            if self.match(current):
                matches.append(current)

            current = next(fib)

        return sum(matches)
