"""
If we list all the natural numbers below 10 that are multiples
of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""

class Main:

    """
    Our constraints are as follows:
        Limit:
            Our task is to find the sum of all integers
            BELOW 1000, so our limit is at max 999

        Divisors:
            We have been given the devisors of 3 and 5,
            but for completeness we'll assume that these
            can be replaced with any number of positive
            integers.
    """
    def __init__(self, limit, divisors):
        self.limit = limit
        self.divisors = divisors
        self.multiples = []

    def answer(self):
        """ Get problem answer """

        self.multiples = []

        for divisor in self.divisors:
            self.find_multiples(divisor)

        return sum(self.multiples)

    def find_multiples(self, divisor):
        """
        Append multiples to the current
        multiples or the specified divisor
        """

        i = divisor

        while i < self.limit:
            if i not in self.multiples:
                self.multiples.append(i)

            i += divisor
